package projects;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.Project2Page;
import pages.Project3Page;
import scripts.Base;
import sun.util.calendar.BaseCalendar;
import utils.DropdownHandler;
import utils.Waiter;

import java.sql.Time;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Project03 extends Base {

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend/project-3");
        project3Page = new Project3Page();
    }

    @Test(priority = 1, description = "Validating the elements in the form")
    public void validateDefaultForm(){
        Assert.assertTrue(project3Page.oneWayRadioButton.isDisplayed());
        Assert.assertTrue(project3Page.oneWayRadioButton.isEnabled());
        Assert.assertTrue(project3Page.oneWayRadioButtonSelector.isSelected());

        Assert.assertTrue(project3Page.roundTripRadioButton.isDisplayed());
        Assert.assertTrue(project3Page.roundTripRadioButton.isEnabled());
        Assert.assertFalse(project3Page.roundTripRadioButtonSelector.isSelected());

        Assert.assertTrue(project3Page.cabinClassLabel.isDisplayed());
        Assert.assertTrue(project3Page.cabinClassDropdown.isDisplayed());

        Assert.assertTrue(project3Page.fromLabel.isDisplayed());
        Assert.assertTrue(project3Page.fromDropdown.isDisplayed());

        Assert.assertTrue(project3Page.toLabel.isDisplayed());
        Assert.assertTrue(project3Page.toDropdown.isDisplayed());

        Assert.assertTrue(project3Page.departDateLabel.isDisplayed());
        Assert.assertTrue(project3Page.departDateSelector.isDisplayed());

        Assert.assertTrue(project3Page.returnDateLabel.isDisplayed());
        Assert.assertTrue(project3Page.returnDateSelector1.isDisplayed());
        Assert.assertFalse(project3Page.returnDateSelector2.isEnabled());

        Assert.assertTrue(project3Page.numberOfPassengersLabel.isDisplayed());
        Assert.assertTrue(project3Page.numberOfPassengersDropdown.isDisplayed());
        //Assert.assertEquals(project3Page.numberOfPassengersDropdown.getText(), "1");

        Assert.assertTrue(project3Page.passengerOneLabel.isDisplayed());
        Assert.assertTrue(project3Page.passengerOneDropdown.isDisplayed());
        //Assert.assertEquals(project3Page.passengerOneDropdown.getAttribute("value"), "Adult (16-64)");

        Assert.assertTrue(project3Page.bookButton.isDisplayed());
        Assert.assertTrue(project3Page.bookButton.isEnabled());
    }

    @Test(priority = 2, description = "Validate the book your trip form when round trip is selected")
    public void validateRoundTrip(){
        project3Page.roundTripRadioButtonSelector.click();
        Assert.assertFalse(project3Page.oneWayRadioButtonSelector.isSelected());

        Assert.assertTrue(project3Page.cabinClassLabel.isDisplayed());
        Assert.assertTrue(project3Page.cabinClassDropdown.isDisplayed());

        Assert.assertTrue(project3Page.fromLabel.isDisplayed());
        Assert.assertTrue(project3Page.fromDropdown.isDisplayed());

        Assert.assertTrue(project3Page.toLabel.isDisplayed());
        Assert.assertTrue(project3Page.toDropdown.isDisplayed());

        Assert.assertTrue(project3Page.departDateLabel.isDisplayed());
        Assert.assertTrue(project3Page.departDateSelector.isDisplayed());

        Assert.assertTrue(project3Page.returnDateLabel.isDisplayed());
        Assert.assertTrue(project3Page.returnDateSelector1.isDisplayed());

        Assert.assertTrue(project3Page.numberOfPassengersLabel.isDisplayed());
        Assert.assertTrue(project3Page.numberOfPassengersDropdown.isDisplayed());
        //Assert.assertEquals(project3Page.numberOfPassengersDropdown.getText(), "1");

        Assert.assertTrue(project3Page.passengerOneLabel.isDisplayed());
        Assert.assertTrue(project3Page.passengerOneDropdown.isDisplayed());
        //Assert.assertEquals(project3Page.passengerOneDropdown.getAttribute("value"), "Adult (16-64)");

        Assert.assertTrue(project3Page.bookButton.isDisplayed());
        Assert.assertTrue(project3Page.bookButton.isEnabled());
    }

    @Test(priority = 3, description = "Validate for 1 passenger 1 way")
    public void validateOnePassengerOneWay(){
        LocalDate currentDate = LocalDate.now();
        int day = currentDate.getDayOfMonth() + 7;
        int month = currentDate.getMonth().getValue();
        int year = currentDate.getYear();

        String futureDate = String.valueOf(currentDate.plusDays(7).format(DateTimeFormatter.ofPattern("MMM dd yyyy")));
        String dayOfTheWeek = String.valueOf(currentDate.plusDays(7).getDayOfWeek());
        String partial = dayOfTheWeek.charAt(0) + dayOfTheWeek.substring(1, 3).toLowerCase();

        String totalDate = partial + " " + futureDate;


        if (day > 31){
            day = day - 31;
            month ++;
            if(month > 12){
                month = month - 12;
                year++;
            }
        }
        project3Page.oneWayRadioButtonSelector.click();

        //project3Page.cabinClassDropdown.click();
        DropdownHandler.selectByIndex(project3Page.cabinClassDropdown, 2);
        Waiter.pause(2);

        DropdownHandler.selectByVisibleText(project3Page.fromDropdown, "Illinois");

        DropdownHandler.selectByVisibleText(project3Page.toDropdown, "Florida");

        project3Page.departDateClearBox.click();
        project3Page.departDateMonthBox.sendKeys(String.valueOf(month));
        project3Page.departDateDayBox.sendKeys(String.valueOf(day));
        project3Page.departDateYearBox.sendKeys(String.valueOf(year));


        DropdownHandler.selectByIndex(project3Page.numberOfPassengersDropdown, 0);
        DropdownHandler.selectByIndex(project3Page.passengerOneDropdown, 1);

        project3Page.bookButton.click();

        Assert.assertEquals(project3Page.successfulDepartLabel.getText(), "DEPART");

        Assert.assertEquals(project3Page.fromAndTo.getText(), "IL to FL");

        Assert.assertEquals(project3Page.leaveByDate.getText(), totalDate);

        Assert.assertEquals(project3Page.successfulNumberOfPassengers.getText(), "Number of Passengers: 1");

        Assert.assertEquals(project3Page.firstPassenger.getText(), "Passenger 1: Senior (65+)");

        Assert.assertEquals(project3Page.successfulCabinClass.getText(),"Cabin class: Business");
    }

    @Test(priority = 4, description = "Validating a test case for one passenger and a round trip ticket")
    public void validateOnePassengerRoundTrip(){
        LocalDate currentDate = LocalDate.now();
        int day1 = currentDate.getDayOfMonth() + 7;
        int month1 = currentDate.getMonth().getValue();
        int year1 = currentDate.getYear();

        int day2 = currentDate.getDayOfMonth() + 31;
        int month2 = currentDate.getMonth().getValue();
        int year2 = currentDate.getYear();

        String futureDate1 = String.valueOf(currentDate.plusDays(7).format(DateTimeFormatter.ofPattern("MMM dd yyyy")));
        String dayOfTheWeek1 = String.valueOf(currentDate.plusDays(7).getDayOfWeek());
        String partial1 = dayOfTheWeek1.charAt(0) + dayOfTheWeek1.substring(1, 3).toLowerCase();

        String totalDate1 = partial1 + " " + futureDate1;

        String futureDate2 = String.valueOf(currentDate.plusDays(31).format(DateTimeFormatter.ofPattern("MMM dd yyyy")));
        String dayOfTheWeek2 = String.valueOf(currentDate.plusDays(31).getDayOfWeek());
        String partial2 = dayOfTheWeek2.charAt(0) + dayOfTheWeek2.substring(1, 3).toLowerCase();

        String totalDate2 = partial2 + " " + futureDate2;


        if (day1 > 31){
            day1 = day1 - 31;
            month1 ++;
            if(month1 > 12){
                month1 = month1 - 12;
                year1++;
            }
        }

        if (day2 > 31){
            day2 = day2 - 30;
            month2 ++;
            if(month2 > 12){
                month2 = month2 - 12;
                year2++;
            }
        }


        project3Page.roundTripRadioButtonSelector.click();

        DropdownHandler.selectByVisibleText(project3Page.cabinClassDropdown, "First");

        DropdownHandler.selectByVisibleText(project3Page.fromDropdown, "California");

        DropdownHandler.selectByVisibleText(project3Page.toDropdown, "Illinois");

        project3Page.departDateClearBox.click();
        project3Page.departDateMonthBox.sendKeys(String.valueOf(month1));
        project3Page.departDateDayBox.sendKeys(String.valueOf(day1));
        project3Page.departDateYearBox.sendKeys(String.valueOf(year1));

        project3Page.returnDateClearBox.click();
        project3Page.returnDateMonthBox.sendKeys(String.valueOf(month2));
        project3Page.returnDateDayBox.sendKeys(String.valueOf(day2));
        project3Page.returnDateYearBox.sendKeys(String.valueOf(year2));

        DropdownHandler.selectByIndex(project3Page.numberOfPassengersDropdown, 0);
        DropdownHandler.selectByIndex(project3Page.passengerOneDropdown, 0);

        project3Page.bookButton.click();

        Assert.assertEquals(project3Page.successfulDepartLabel.getText(), "DEPART");

        Assert.assertEquals(project3Page.fromAndTo.getText(), "CA to IL");

        Assert.assertEquals(project3Page.leaveByDate.getText(), totalDate1);

        Assert.assertEquals(project3Page.successfulNumberOfPassengers.getText(), "Number of Passengers: 1");

        Assert.assertEquals(project3Page.firstPassenger.getText(), "Passenger 1: Adult (16-64)");

        Assert.assertEquals(project3Page.successfulCabinClass.getText(),"Cabin class: First");

        Assert.assertEquals(project3Page.successfulReturnLabel.getText(), "RETURN");

        Assert.assertEquals(project3Page.returnToAndFrom.getText(), "IL to CA");

        Assert.assertEquals(project3Page.returnDate.getText(), totalDate2);
    }

    @Test(priority = 5, description = "Validating a test case for two passengers going one way")
    public void validateTwoPassengersOneWay(){
        LocalDate currentDate = LocalDate.now();
        int day = currentDate.getDayOfMonth() + 1;
        int month = currentDate.getMonth().getValue();
        int year = currentDate.getYear();

        String futureDate = String.valueOf(currentDate.plusDays(1).format(DateTimeFormatter.ofPattern("MMM dd yyyy")));
        String dayOfTheWeek = String.valueOf(currentDate.plusDays(1).getDayOfWeek());
        String partial = dayOfTheWeek.charAt(0) + dayOfTheWeek.substring(1, 3).toLowerCase();

        String totalDate = partial + " " + futureDate;


        if (day > 31){
            day = day - 31;
            month ++;
            if(month > 12){
                month = month - 12;
                year++;
            }
        }

        project3Page.oneWayRadioButtonSelector.click();

        DropdownHandler.selectByVisibleText(project3Page.cabinClassDropdown, "Premium Economy");

        DropdownHandler.selectByVisibleText(project3Page.fromDropdown, "New York");

        DropdownHandler.selectByVisibleText(project3Page.toDropdown, "Texas");

        project3Page.departDateClearBox.click();
        project3Page.departDateMonthBox.sendKeys(String.valueOf(month));
        project3Page.departDateDayBox.sendKeys(String.valueOf(day));
        project3Page.departDateYearBox.sendKeys(String.valueOf(year));

        DropdownHandler.selectByVisibleText(project3Page.numberOfPassengersDropdown, "2");
        DropdownHandler.selectByVisibleText(project3Page.passengerOneDropdown, "Adult (16-64)");
        DropdownHandler.selectByVisibleText(project3Page.passengerTwoDropdown, "Child (2-11)");

        project3Page.bookButton.click();

        Assert.assertEquals(project3Page.successfulDepartLabel.getText(), "DEPART");

        Assert.assertEquals(project3Page.fromAndTo.getText(), "NY to TX");

        Assert.assertEquals(project3Page.leaveByDate.getText(), totalDate);

        Assert.assertEquals(project3Page.successfulNumberOfPassengers.getText(), "Number of Passengers: 2");

        Assert.assertEquals(project3Page.firstPassenger.getText(), "Passenger 1: Adult (16-64)");

        Assert.assertEquals(project3Page.secondPassenger.getText(), "Passenger 2: Child (2-11)");

        Assert.assertEquals(project3Page.successfulCabinClassWithTwoPassengers.getText(), "Cabin class: Premium Economy");



    }


}
