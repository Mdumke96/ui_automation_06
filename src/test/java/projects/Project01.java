package projects;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import scripts.Base;
import utils.Waiter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Project01 extends Base {

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend/project-1");
    }

    @Test
    public void validateContactUs(){
        WebElement header = driver.findElement(By.cssSelector("h1[class^='is-size-2']"));
        Assert.assertTrue(header.isDisplayed());
        Assert.assertEquals(header.getText(), "Contact Us");
        WebElement address = driver.findElement(By.id("address"));
        Assert.assertEquals(address.getText(), "2860 S River Rd Suite 350, Des Plaines IL 60018");
        WebElement email = driver.findElement(By.id("email"));
        Assert.assertEquals(email.getText(), "info@techglobalschool.com");
        WebElement phoneNumber = driver.findElement(By.id("phone-number"));
        Assert.assertEquals(phoneNumber.getText(), "(773) 257-3010");
    }

    @Test
    public void validateFullNameInputBox(){
        WebElement fullNameInputBox = driver.findElement(By.cssSelector("input[class*='input'] "));
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        Assert.assertTrue(fullNameInputBox.isDisplayed());
        submitButton.click();
        WebElement fullNameInputBoxTitle = driver.findElement(By.cssSelector("label[for*='name']"));
        Assert.assertEquals(fullNameInputBoxTitle.getText(),"Full name *");
        Assert.assertEquals(fullNameInputBox.getAttribute("placeholder"), "Enter your full name");
    }

    @Test
    public void validateGenderRadioButton(){
        List<WebElement> labels = driver.findElements(By.cssSelector("label[class*='label']"));
        Assert.assertEquals(labels.get(1).getText(), "Gender *");
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        submitButton.click();
        List<WebElement> genders = driver.findElements(By.cssSelector("label[class*='radio']"));
        Assert.assertEquals(genders.get(0).getText(), "Male");
        Assert.assertEquals(genders.get(1).getText(), "Female");
        Assert.assertEquals(genders.get(2).getText(), "Prefer not to disclose");
        List<WebElement> genderButtons = driver.findElements(By.cssSelector("input[type*='radio']"));
        for(int i = 0; i < genderButtons.size(); i++){
            Assert.assertFalse(genderButtons.get(i).isSelected());
            genderButtons.get(i).click();
        }
    }

    @Test
    public void validateAddressInputBox(){
        WebElement addressInputBox = driver.findElement(By.cssSelector("input[placeholder*='address']"));
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        Assert.assertTrue(addressInputBox.isDisplayed());
        submitButton.click();
        List<WebElement> labels = driver.findElements(By.cssSelector("label[class*='label']"));
        Assert.assertEquals(labels.get(2).getText(), "Address");
        Assert.assertEquals(addressInputBox.getAttribute("placeholder"), "Enter your address");
    }

    @Test
    public void validateEmailInputBox(){
        WebElement emailInputBox = driver.findElement(By.cssSelector("input[placeholder*='email']"));
        Assert.assertTrue(emailInputBox.isDisplayed());
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        submitButton.click();
        List<WebElement> labels = driver.findElements(By.cssSelector("label[class*='label']"));
        Assert.assertEquals(labels.get(3).getText(), "Email *");
        Assert.assertEquals(emailInputBox.getAttribute("placeholder"), "Enter your email");
    }

    @Test
    public void validatePhoneInputBox(){
        WebElement phoneInputBox = driver.findElement(By.cssSelector("input[placeholder*='phone']"));
        Assert.assertTrue(phoneInputBox.isDisplayed());
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        submitButton.click();
        List<WebElement> labels = driver.findElements(By.cssSelector("label[class*='label']"));
        Assert.assertEquals(labels.get(4).getText(), "Phone");
        Assert.assertEquals(phoneInputBox.getAttribute("placeholder"), "Enter your phone number");
    }

    @Test
    public void validateMessageTextBox(){
        WebElement messageTextBox = driver.findElement(By.cssSelector("textarea[class*='textarea']"));
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        submitButton.click();
        List<WebElement> labels = driver.findElements(By.cssSelector("label[class*='label']"));
        Assert.assertEquals(labels.get(5).getText(), "Message");
        Assert.assertEquals(messageTextBox.getAttribute("placeholder"), "Type your message here...");
    }

    @Test
    public void validateConsentCheckbox(){
        WebElement consentCheckbox = driver.findElement(By.cssSelector("input[type*='checkbox']"));
        WebElement consentCheckboxText = driver.findElement(By.cssSelector("label[class*='checkbox']"));
        Assert.assertEquals(consentCheckboxText.getText(), "I give my consent to be contacted.");
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        submitButton.click();
       consentCheckbox.click();
       Assert.assertTrue(consentCheckbox.isSelected());
       consentCheckbox.click();
       Assert.assertFalse(consentCheckbox.isSelected());
    }

    @Test
    public void validateSubmitButton(){
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        Assert.assertTrue(submitButton.isDisplayed());
        submitButton.click();
        Assert.assertEquals(submitButton.getText(),"SUBMIT");
    }
    @Test
    public void validateFormSubmission(){
        WebElement fullNameInputBox = driver.findElement(By.cssSelector("input[class*='input'] "));
        fullNameInputBox.sendKeys("John Doe");
        List<WebElement> genders = driver.findElements(By.cssSelector("input[type*='radio']"));
        genders.get(0).click();
        WebElement addressInputBox = driver.findElement(By.cssSelector("input[placeholder*='address']"));
        addressInputBox.sendKeys("123 chicago ave");
        WebElement emailInputBox = driver.findElement(By.cssSelector("input[placeholder*='email']"));
        emailInputBox.sendKeys("JohnDoe@gmail.com");
        WebElement phoneInputBox = driver.findElement(By.cssSelector("input[placeholder*='phone']"));
        phoneInputBox.sendKeys("123-123-1234");
        WebElement messageTextBox = driver.findElement(By.cssSelector("textarea[class*='textarea']"));
        messageTextBox.sendKeys("Hello World");
        WebElement consentCheckbox = driver.findElement(By.cssSelector("label[class*='checkbox']"));
        consentCheckbox.click();
        WebElement submitButton = driver.findElement(By.cssSelector("button[class*='button'] "));
        submitButton.click();
        WebElement submitMessage = driver.findElement(By.cssSelector("strong[class*='mt-5']"));
        Assert.assertEquals(submitMessage.getText(),"Thanks for submitting!");
    }
}
