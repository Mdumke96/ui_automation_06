package projects;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import javafx.scene.layout.Priority;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.Project2Page;
import scripts.Base;
import utils.Waiter;

public class Project02 extends Base {

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend/project-2");
        project2Page = new Project2Page();
    }

    @Test(priority = 1, description = "Test Case 01 - Validate the login form")
    public void validateTheLoginForm(){
        Assert.assertTrue(project2Page.usernameInputBox.isDisplayed());
        Assert.assertTrue(project2Page.usernameInputBox.isEnabled());
        Assert.assertNotEquals(project2Page.usernameInputBox.getAttribute("required"), "true");
        Assert.assertTrue(project2Page.usernameInputBoxLabel.isDisplayed());
        Assert.assertEquals(project2Page.usernameInputBoxLabel.getText(), "Please enter your username");

        Assert.assertTrue(project2Page.passwordInputBox.isDisplayed());
        Assert.assertTrue(project2Page.passwordInputBox.isEnabled());
        Assert.assertNotEquals(project2Page.passwordInputBox.getAttribute("required"), "true");
        Assert.assertTrue(project2Page.passwordInputBoxLabel.isDisplayed());
        Assert.assertEquals(project2Page.passwordInputBoxLabel.getText(), "Please enter your password");

        Assert.assertTrue(project2Page.loginButton.isDisplayed());
        Assert.assertTrue(project2Page.loginButton.isEnabled());
        Assert.assertEquals(project2Page.loginButton.getText(), "LOGIN");

        Assert.assertTrue(project2Page.forgotPasswordLink.isDisplayed());
        Assert.assertTrue(project2Page.forgotPasswordLink.isEnabled());
        Assert.assertEquals(project2Page.forgotPasswordLink.getText(), "Forgot Password?");
    }

    @Test
    public void validateValidLogin(){
        WebElement usernameInputBox = driver.findElement(By.id("username"));
        WebElement passwordInputBox = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.id("login_btn"));

        usernameInputBox.sendKeys("TechGlobal");
        passwordInputBox.sendKeys("Test1234");
        loginButton.click();

        WebElement successMessage = driver.findElement(By.id("success_lgn"));
        Assert.assertTrue(successMessage.isDisplayed());
        Assert.assertEquals(successMessage.getText(), "You are logged in");

        WebElement logOutButton = driver.findElement(By.id("logout"));
        Assert.assertTrue(logOutButton.isDisplayed());
        Assert.assertEquals(logOutButton.getText(), "LOGOUT");
    }

    @Test(priority = 2, description = "Test Case 02 - Validate the valid login")
    public void validateTheValidLogin(){
        project2Page.login("TechGlobal", "Test1234");

        Assert.assertTrue(project2Page.successfulLoginMessage.isDisplayed());
        Assert.assertEquals(project2Page.successfulLoginMessage.getText(), "You are logged in");

        Assert.assertTrue(project2Page.logoutButton.isDisplayed());
        Assert.assertTrue(project2Page.logoutButton.isEnabled());
        Assert.assertEquals(project2Page.logoutButton.getText(), "LOGOUT");
    }

    @Test()
    public void validateTheLogout(){
        project2Page.login("TechGlobal", "Test1234");
        project2Page.logoutButton.click();
    }


    @Test(priority = 4, description = "Test case 03 - Validate the forgotten password")
    public void validateForgotPassword(){
        project2Page.forgotPasswordLink.click();

        Assert.assertTrue(project2Page.resetPasswordModalHeading.isDisplayed());
        Assert.assertEquals(project2Page.resetPasswordModalHeading.getText(), "Reset Password");

        Assert.assertTrue(project2Page.resetPasswordModalCloseButton.isDisplayed());

        Assert.assertTrue(project2Page.resetPasswordModalEmailInputBox.isDisplayed());

        Assert.assertEquals(project2Page.resetPasswordModalEmailInputBoxLabel.getText(), "Enter your email address and we'll send you a link to reset your password.");

        Assert.assertTrue(project2Page.resetPasswordModalEmailInputBox.isDisplayed());
        Assert.assertTrue(project2Page.resetPasswordModalEmailInputBox.isEnabled());
        Assert.assertEquals(project2Page.resetPasswordModalSubmitButton.getText(), "SUBMIT");
    }

    @Test(priority = 5, description = "")
    public void validateResetPassword() {
        project2Page.forgotPasswordLink.click();

        Assert.assertTrue(project2Page.resetPasswordModal.isDisplayed());

        project2Page.resetPasswordModalCloseButton.click();

        try{
           Assert.assertFalse(project2Page.resetPasswordModal.isDisplayed());
        }
        catch(StaleElementReferenceException e){
            Assert.assertTrue(true);
        }
    }

    @Test
    public void validateFormSubmission(){
        project2Page.forgotPasswordLink.click();

        project2Page.resetPasswordModalEmailInputBox.sendKeys("JohnDoe@gmail.com");

        project2Page.resetPasswordModalSubmitButton.click();

        Assert.assertEquals(project2Page.resetPasswordModalMessage.getText(), "A link to reset your password has been sent to your email address.");
    }

    @Test
    public void validateEmptyCredentials(){
        WebElement loginButton = driver.findElement(By.id("login_btn"));
        loginButton.click();

        WebElement errorMessage = driver.findElement(By.id("error_message"));
        Assert.assertEquals(errorMessage.getText(), "Invalid Username entered!");
    }

    @Test
    public void validateWrongUsername(){
        WebElement usernameInputBox = driver.findElement(By.id("username"));
        WebElement passwordInputBox = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.id("login_btn"));

        usernameInputBox.sendKeys("John");
        passwordInputBox.sendKeys("Test1234");
        loginButton.click();

        WebElement errorMessage = driver.findElement(By.id("error_message"));
        Assert.assertEquals(errorMessage.getText(), "Invalid Username entered!");
    }

    @Test
    public void validateWrongPassword(){
        WebElement usernameInputBox = driver.findElement(By.id("username"));
        WebElement passwordInputBox = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.id("login_btn"));

        usernameInputBox.sendKeys("TechGlobal");
        passwordInputBox.sendKeys("1234");
        loginButton.click();

        WebElement errorMessage = driver.findElement(By.id("error_message"));
        Assert.assertEquals(errorMessage.getText(), "Invalid Password entered!");
    }

    @Test
    public void validateWrongUsernameAndPassword(){
        WebElement usernameInputBox = driver.findElement(By.id("username"));
        WebElement passwordInputBox = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.id("login_btn"));

        usernameInputBox.sendKeys("John");
        passwordInputBox.sendKeys("1234");
        loginButton.click();

        WebElement errorMessage = driver.findElement(By.id("error_message"));
        Assert.assertEquals(errorMessage.getText(), "Invalid Username entered!");
    }

}
