package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Scanner;

public class _15_TGFileUpload extends Base{

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend");
        driver.findElement(By.id("card-13")).click();
    }

    @Test
    public void basicFileUpload(){
        WebElement fileUploadInput = driver.findElement(By.id("file_upload"));
        WebElement submitButton = driver.findElement(By.id("file_submit"));

        String filePath = "C:\\Users\\keith\\IdeaProjects\\ui_automation_6\\hello.txt";

        fileUploadInput.sendKeys(filePath);
        submitButton.click();

        WebElement resultText = driver.findElement(By.cssSelector("p[id*='result']"));

        Assert.assertEquals(resultText.getText(), "You Uploaded '" +
                filePath.substring(filePath.lastIndexOf('\\') + 1) + "'");
    }



}
