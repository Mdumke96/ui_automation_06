package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.Waiter;

public class _16_TGFileDownload extends Base{

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend");
        driver.findElement(By.id("card-14")).click();
    }

    @Test
    public void validateFileDownload(){
        WebElement fileDownload = driver.findElement(By.id("file_download"));
        fileDownload.click();
        Waiter.pause(5);
    }
}
