package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class _04_TGLocatorsTest extends Base{
    /*
    Test case 1:
    Go to "https://techglobal-training.com/frontend/"
    Click on "Xpath-CSS Locators" card
    Validate the header is "Xpath-CSS Locators"
     */

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend/");
    }

    @Test
    public void validateTheHeader() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.id("card-2")).click();
        Thread.sleep(2000);
        WebElement header = driver.findElement(By.cssSelector("#main_heading"));

        Assert.assertTrue(header.isDisplayed());
        Assert.assertEquals(header.getText(), "Xpath-CSS Locators");
    }


    /*
    Test case 2:
    Go to "https://techglobal-training.com/frontend/"
    Click on "Xpath-CSS Locators" card
    Validate the languages are "Java", "JavaScript", "C#"
     */

    @Test
    public void validateLanguages(){
        driver.findElement(By.id("card-2")).click();
        List<WebElement> languages = driver.findElements(By.xpath("(//h3[@class='Xpath_subheader__VOtsU'])[1]/../ul/li"));

        String[] expectedTexts = {"Java", "JavaScript", "C#"};

        for(int i = 0; i < 3; i++){
            Assert.assertTrue(languages.get(i).isDisplayed());
            Assert.assertEquals(languages.get(i).getText(), expectedTexts[i]);

        }
    }

     /*
    Test case 3:
    Go to "https://techglobal-training.com/frontend/"
    Click on "Xpath-CSS Locators" card
    Validate the Automation tools are "Selenium WebDriver", "WebDriverIO", "Cypress", and ""
     */

    @Test
    public void validateAutomationTools(){
        driver.findElement(By.id("card-2")).click();
        List<WebElement> languages = driver.findElements(By.xpath("(//h3[@class='Xpath_subheader__VOtsU'])[2]/..//li"));

        String[] expectedTexts = {"Selenium WebDriver", "WebDriverIO", "Cypress", "Playwright"};

        for(int i = 0; i < 4; i++){
            Assert.assertTrue(languages.get(i).isDisplayed());
            Assert.assertEquals(languages.get(i).getText(), expectedTexts[i]);

        }
    }

         /*
    Test case 4:
    Go to "https://techglobal-training.com/frontend/"
    Click on "Xpath-CSS Locators" card
    Validate the Headings are "Programming Languages", "Automation Tools"
     */

    @Test
    public void validateListHeaders(){
        driver.findElement(By.id("card-2")).click();
        List<WebElement> headers = driver.findElements(By.tagName("h3"));

        String[] expectedTexts = {"Programming Languages", "Automation Tools"};

        for(int i = 0; i < headers.size(); i++){
            Assert.assertTrue(headers.get(i).isDisplayed());
            Assert.assertEquals(headers.get(i).getText(), expectedTexts[i]);

        }
    }


}
