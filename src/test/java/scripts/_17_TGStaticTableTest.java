package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.TableHandler;

import javax.xml.ws.WebEndpoint;
import java.util.List;

public class _17_TGStaticTableTest extends Base{

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend");
        driver.findElement(By.id("card-11")).click();
    }


    @Test
    public void validateTableHeader(){
        List<WebElement> tableHeaders = driver.findElements(By.cssSelector(".header"));
        String[] expectedResults = {"Rank", "Company", "Employees", "Country"};

        for(int i = 0; i < tableHeaders.size(); i++){
            Assert.assertEquals(tableHeaders.get(i).getText(), expectedResults[i]);
        }
    }


    @Test
    public void validateFirstRow(){
        List<WebElement> rowOne = TableHandler.getTableRow(1);
        String[] expectedResults = {"1", "Amazon", "1,523,000", "USA"};

        for(int i = 0; i < rowOne.size(); i++){
            Assert.assertEquals(rowOne.get(i).getText(), expectedResults[i]);
        }
    }

    @Test
    public void validateLastColumn(){
        List<WebElement> lastColumn = TableHandler.getTableColumn(4);
        String[] expectedResults = {"USA", "China", "USA", "USA", "S. Korea"};

        for(int i = 0; i < lastColumn.size(); i++){
            Assert.assertEquals(lastColumn.get(i).getText(), expectedResults[i]);
        }
    }

    @Test
    public void validateEachCell(){
        WebElement mainTable = driver.findElement(By.id("company_table"));
        List<List<WebElement>> tableData = TableHandler.getTableData(mainTable);

        for(List<WebElement> row : tableData){
            for(WebElement cell : row){
                System.out.println(cell.getText());
            }
        }
    }

}
