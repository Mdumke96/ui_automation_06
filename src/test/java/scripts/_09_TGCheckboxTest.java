package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class _09_TGCheckboxTest extends Base{

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend/");
        driver.findElement(By.id("card-7")).click();
    }

    @Test
    public void validateAppleAndMicrosoft(){
        List<WebElement> checkboxLabel = driver.findElements(By.cssSelector("#checkbox-button-group_1 input"));

        for (int i = 0; i < checkboxLabel.size(); i++) {
            Assert.assertTrue(checkboxLabel.get(i).isEnabled());
            Assert.assertTrue(checkboxLabel.get(i).isDisplayed());
            Assert.assertFalse(checkboxLabel.get(i).isSelected());
            checkboxLabel.get(i).click();
        }
        for(int i = 0; i < checkboxLabel.size(); i++ ){
            Assert.assertTrue(checkboxLabel.get(i).isSelected());
            checkboxLabel.get(i).click();
        }
        for(int i = 0; i < checkboxLabel.size(); i++ ){
            Assert.assertFalse(checkboxLabel.get(i).isSelected());
            checkboxLabel.get(i).click();
        }
    }
}
