package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class _07_TGDynamicElementsTest extends Base {

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend/");
        driver.findElement(By.id("card-3")).click();
    }

    /**
     * TEST CASE 1
     * Go to https://techglobal-training.com/frontend/
     * Click on the "Dynamic Elements" card
     * Validate box1 is displayed with the below texts
     * Box 1
     */

    @Test
    public void validateDynamicElementBox1(){
        WebElement box1 = driver.findElement(By.cssSelector("p[id^='box_1_']"));

        Assert.assertTrue(box1.isDisplayed());
        Assert.assertEquals(box1.getText(), "Box 1");
    }

    @Test
    public void validateDynamicElementBox2(){
        WebElement box2 = driver.findElement(By.cssSelector("p[id^='box_2_']"));

        Assert.assertTrue(box2.isDisplayed());
        Assert.assertEquals(box2.getText(), "Box 2");
    }

    @Test
    public void validateDynamicElementBoxes(){
        String[] arr = {"Box 1", "Box 2"};

        List<WebElement> list = driver.findElements(By.cssSelector("p[id*='box_']"));

        for(int i = 0; i < list.size(); i++){
            Assert.assertTrue(list.get(i).isDisplayed());
            Assert.assertEquals(list.get(i).getText(), arr[i]);
        }
    }
}
