package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

import java.security.cert.CertStoreSpi;

public class Project3Page {

    public Project3Page(){
        PageFactory.initElements(Driver.getDriver(), this);}

    @FindBy(css = "label[class*='radio ml-0']")
    public WebElement oneWayRadioButton;

    @FindBy(css = "div > div > label.radio.ml-0 > input")
    public WebElement oneWayRadioButtonSelector;

    @FindBy(css = "div > label:nth-child(2)")
    public WebElement roundTripRadioButton;

    @FindBy(css = "div > div > label:nth-child(2) > input")
    public WebElement roundTripRadioButtonSelector;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(2)")
    public WebElement cabinClassLabel;

    @FindBy(css = "div:nth-child(2) > div > select")
    public WebElement cabinClassDropdown;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(3)")
    public WebElement fromLabel;

    @FindBy(css = "div:nth-child(3) > div > select")
    public WebElement fromDropdown;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(4)")
    public WebElement toLabel;

    @FindBy(css = "div:nth-child(4) > div > select")
    public WebElement toDropdown;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(5)")
    public WebElement departDateLabel;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(5) > div")
    public WebElement departDateSelector;

    @FindBy(css = "div:nth-child(5) > div > div > div > button.react-date-picker__clear-button.react-date-picker__button > svg")
    public WebElement departDateClearBox;

    @FindBy(css = "div:nth-child(5) > div > div > div > div > input.react-date-picker__inputGroup__input.react-date-picker__inputGroup__month")
    public WebElement departDateMonthBox;

    @FindBy(css = "div:nth-child(5) > div > div > div > div > input.react-date-picker__inputGroup__input.react-date-picker__inputGroup__day")
    public WebElement departDateDayBox;

    @FindBy(css = "div:nth-child(5) > div > div > div > div > input.react-date-picker__inputGroup__input.react-date-picker__inputGroup__year")
    public WebElement departDateYearBox;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(6)")
    public WebElement returnDateLabel;

    @FindBy(css = "div:nth-child(6) > div > div > div > button.react-date-picker__clear-button.react-date-picker__button > svg")
    public WebElement returnDateClearBox;

    @FindBy(css = "div:nth-child(6) > div > div > div > div > input.react-date-picker__inputGroup__input.react-date-picker__inputGroup__month")
    public WebElement returnDateMonthBox;

    @FindBy(css = "div:nth-child(6) > div > div > div > div > input.react-date-picker__inputGroup__input.react-date-picker__inputGroup__day")
    public WebElement returnDateDayBox;

    @FindBy(css = "div:nth-child(6) > div > div > div > div > input.react-date-picker__inputGroup__input.react-date-picker__inputGroup__year")
    public WebElement returnDateYearBox;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(6) > div > div")
    public WebElement returnDateSelector1;

    @FindBy(css = "div:nth-child(6) > div > div > div > div > input[type=date]:nth-child(1)")
    public WebElement returnDateSelector2;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(7)")
    public WebElement numberOfPassengersLabel;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(7) > div > select")
    public WebElement numberOfPassengersDropdown;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(7) > label")
    public WebElement passengerOneLabel;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(8) > div > select")
    public WebElement passengerOneDropdown;

    @FindBy(css = "form > div.Projects_container__g8xeT > div:nth-child(9) > div > select")
    public WebElement passengerTwoDropdown;

    @FindBy(css = "div:nth-child(2) > div > button")
    public WebElement bookButton;

    @FindBy(css = " form > div.ml-3 > div.field.is-flex > div > h1")
    public WebElement successfulDepartLabel;

    @FindBy(css = "form > div.ml-3 > div.field.is-flex > div > h3")
    public WebElement fromAndTo;

    @FindBy(css = "form > div.ml-3 > div.field.is-flex > div > p")
    public WebElement leaveByDate;

    @FindBy(css = "form > div.ml-3 > div.mt-4.is-family-monospace.has-text-black > p:nth-child(1)")
    public WebElement successfulNumberOfPassengers;

    @FindBy(css = "form > div.ml-3 > div.mt-4.is-family-monospace.has-text-black > p:nth-child(2)")
    public WebElement firstPassenger;

    @FindBy(css = "form > div.ml-3 > div.mt-4.is-family-monospace.has-text-black > p:nth-child(3)")
    public WebElement secondPassenger;

    @FindBy(css = "form > div.ml-3 > div.mt-4.is-family-monospace.has-text-black > p:nth-child(3)")
    public WebElement successfulCabinClass;

    @FindBy(css = "form > div.ml-3 > div.mt-4.is-family-monospace.has-text-black > p:nth-child(4)")
    public WebElement successfulCabinClassWithTwoPassengers;

    @FindBy(css = "form > div.ml-3 > div.field.is-flex > div:nth-child(2) > div > h1")
    public WebElement successfulReturnLabel;

    @FindBy(css = "form > div.ml-3 > div.field.is-flex > div:nth-child(2) > div > h3")
    public WebElement returnToAndFrom;

    @FindBy(css = "div.ml-3 > div.field.is-flex > div:nth-child(2) > div > p")
    public WebElement returnDate;
}
